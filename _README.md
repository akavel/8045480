# Patch to cross compile to android with net/http support

## Installation instructions

We need to patch Go's source code to allow cgo when cross-compiling and make Android-specific changes since it sometimes differs from linux (e.g.: it doesn't use /etc/resolv.conf for DNS config)

1. Clone the go source: ```hg clone -u release https://code.google.com/p/go```
2. Enter the go source directory: ```cd go```
3. Apply the patch in this gist: ```patch -p1 < /path/to/patch/go_android.patch```
4. Enter the go/src directory: ```cd src```
5. Generate binaries for Android cross-compile: ```GOOS=linux GOARCH=arm ./make.bash```
6. Install standard packages for android: ```GOOS=linux GOARCH=arm CGOENABLED=1 ../bin/go install -tags android -a -v std```

## Example program for Android and running in the device

Requirements: an android device with remote debugging enabled, ```adb``` to build the binary. This doesn't require the device to be rooted.

This example is cross compiling the https://github.com/ernesto-jimenez/emit_urls project.

1. Cross compile the binary: ```GOARM=5 GOOS=linux GOARCH=arm CGOENABLED=1 ../bin/go build -tags android -o main-arm $GOPATH/src/github.com/ernesto-jimenez/emit_urls/main.go```
2. Push binary to the Android device with adb: ```adb push main-arm /data/local/tmp/```
3. Run program in the device: ```adb shell ./data/local/tmp/main-arm http://rediris.es```

## Credits

This is based on the following patch from @minux https://codereview.appspot.com/6454055